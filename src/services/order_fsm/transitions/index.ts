import {OrderState} from "../../../enums/OrderState";
import {OrderFsmEvent} from "../order_fsm_event";

import {OrderFsmTransition} from "../order_fsm_transition";


import {SAVE_ORDER} from "../actions/SAVE_ORDER";
import {RUN_SEARCH_PERFORMERS_WORKER} from "../actions/RUN_SEARCH_PERFORMERS_WORKER";
import {SEND_NOT_FOUND_PUSH} from "../actions/SEND_NOT_FOUND_PUSH";
import {CANCEL_OFFERS} from "../actions/CANCEL_OFFERS";
import {SELECT_PERFORMER} from "../actions/SELECT_PERFORMER";
import {SEND_PERFORMER_SELECTED_PUSH} from "../actions/SEND_PERFORMER_SELECTED_PUSH";
import {SEND_ORDER_RUNNING_PUSH} from "../actions/SEND_ORDER_RUNNING_PUSH";
import {REJECT_MY_OFFER} from "../actions/REJECT_MY_OFFER";
import {SEND_ORDER_DONE_PUSH} from "../actions/SEND_ORDER_DONE_PUSH";
import {CHECK_IS_CUSTOMER} from "../guards/CHECK_IS_CUSTOMER";
import {CHECK_IS_SYSTEM} from "../guards/CHECK_IS_SYSTEM";
import {CHECK_IS_PERFORMER} from "../guards/CHECK_IS_PERFORMER";
import {CHECK_PERFORMER_ID} from "../guards/CHECK_PERFORMER_ID";
import {SEND_FOUND_PERFORMERS_PUSH} from "../actions/SEND_FOUND_PERFORMERS_PUSH";


export const transitions: OrderFsmTransition[] = [
  //  NEW
  {
    source: OrderState.NEW,
    target: OrderState.SEARCHING,
    events: [OrderFsmEvent.ON_SEARCH],
    guards: [CHECK_IS_CUSTOMER],
    actions: [SAVE_ORDER, RUN_SEARCH_PERFORMERS_WORKER]
  },
  {
    source: OrderState.NEW,
    target: OrderState.CANCELED,
    events: [OrderFsmEvent.ON_CANCEL],
    guards: [CHECK_IS_CUSTOMER],
    actions: [SAVE_ORDER]
  },
  //  SEARCHING
  {
    source: OrderState.SEARCHING,
    target: OrderState.PERFORMERS_FOUND,
    events: [OrderFsmEvent.ON_FOUND],
    guards: [CHECK_IS_SYSTEM],
    actions: [SAVE_ORDER, SEND_FOUND_PERFORMERS_PUSH]
  },
  {
    source: OrderState.SEARCHING,
    target: OrderState.NEW,
    events: [OrderFsmEvent.ON_EDIT],
    guards: [CHECK_IS_CUSTOMER],
    actions: [SAVE_ORDER]
  },
  {
    source: OrderState.SEARCHING,
    target: OrderState.NO_PERFORMER,
    events: [OrderFsmEvent.ON_NOT_FOUND],
    guards: [CHECK_IS_SYSTEM],
    actions: [SAVE_ORDER, SEND_NOT_FOUND_PUSH]
  },
  {
    source: OrderState.SEARCHING,
    target: OrderState.CANCELED,
    events: [OrderFsmEvent.ON_CANCEL],
    guards: [CHECK_IS_CUSTOMER],
    actions: [CANCEL_OFFERS, SAVE_ORDER]
  },
  //   PERFORMERS_FOUND
  {
    source: OrderState.PERFORMERS_FOUND,
    target: OrderState.SEARCHING,
    events: [OrderFsmEvent.ON_SEARCH],
    guards: [CHECK_IS_CUSTOMER],
    actions: [SAVE_ORDER, RUN_SEARCH_PERFORMERS_WORKER]
  },
  {
    source: OrderState.PERFORMERS_FOUND,
    target: OrderState.CANCELED,
    events: [OrderFsmEvent.ON_CANCEL],
    guards: [CHECK_IS_CUSTOMER],
    actions: [SAVE_ORDER]
  },
  {
    source: OrderState.PERFORMERS_FOUND,
    events: [OrderFsmEvent.ON_SELECT],
    target: OrderState.SELECTED,
    guards: [CHECK_IS_CUSTOMER, CHECK_PERFORMER_ID],
    actions: [SELECT_PERFORMER, SAVE_ORDER, SEND_PERFORMER_SELECTED_PUSH]
  },

  //  NO_PERFORMER
  {
    source: OrderState.NO_PERFORMER,
    target: OrderState.NEW,
    events: [OrderFsmEvent.ON_EDIT],
    guards: [CHECK_IS_CUSTOMER],
    actions: [SAVE_ORDER]
  },

  {
    source: OrderState.NO_PERFORMER,
    target: OrderState.SEARCHING,
    events: [OrderFsmEvent.ON_SEARCH],
    guards: [CHECK_IS_CUSTOMER],
    actions: [SAVE_ORDER, RUN_SEARCH_PERFORMERS_WORKER]
  },

  //   SELECTED
  {
    source: OrderState.SELECTED,
    target: OrderState.RUNNING,
    events: [OrderFsmEvent.ON_RUN],
    guards: [CHECK_IS_PERFORMER],
    actions: [SAVE_ORDER, SEND_ORDER_RUNNING_PUSH]
  },
  {
    source: OrderState.SELECTED,
    target: OrderState.NO_PERFORMER,
    events: [OrderFsmEvent.ON_REJECT],
    guards: [CHECK_IS_PERFORMER],
    actions: [REJECT_MY_OFFER, SAVE_ORDER]
  },
  {
    source: OrderState.SELECTED,
    target: OrderState.CANCELED,
    events: [OrderFsmEvent.ON_CANCEL],
    guards: [CHECK_IS_CUSTOMER],
    actions: [CANCEL_OFFERS, SAVE_ORDER]
  },
  //  RUNNING
  {
    source: OrderState.RUNNING,
    target: OrderState.DONE,
    events: [OrderFsmEvent.ON_DONE],
    guards: [CHECK_IS_SYSTEM],
    actions: [SAVE_ORDER, SEND_ORDER_DONE_PUSH]
  },
  {
    source: OrderState.RUNNING,
    target: OrderState.NOT_DONE,
    events: [OrderFsmEvent.ON_NOT_DONE],
    guards: [CHECK_IS_SYSTEM],
    actions: [SAVE_ORDER]
  },


];


