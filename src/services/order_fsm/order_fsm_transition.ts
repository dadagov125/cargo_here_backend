import {FsmTransition} from "../fsm/FsmBase";

import {OrderFsmEvent} from "./order_fsm_event";
import {OrderState} from "../../enums/OrderState";
import {Order} from "../../entity/Order";

export type OrderFsmState = OrderState

export interface OrderFsmTransition extends FsmTransition<Order, OrderFsmState, OrderFsmEvent> {

}
