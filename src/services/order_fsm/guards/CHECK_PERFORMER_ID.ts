import {Service} from "@tsed/di";
import {EventArgs, Guard} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";


@Service()

export class CHECK_PERFORMER_ID extends Guard<Order> {
  async check(holder: Order, args: EventArgs): Promise<void> {
    if (!args || !args.data || !args.data.performerId) throw new Error("PerformerId required!");
  }


}
