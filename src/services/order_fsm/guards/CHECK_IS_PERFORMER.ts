import {EventArgs, Guard} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {Service} from "@tsed/di";

@Service()
export class CHECK_IS_PERFORMER extends Guard<Order> {
  async check(order: Order, args: EventArgs): Promise<void> {
    const user = args.user;
    let find = order.offers.find(o => o.performer.id === user.id);
    if (!find) throw new Error('user is not performer');
  }

}
