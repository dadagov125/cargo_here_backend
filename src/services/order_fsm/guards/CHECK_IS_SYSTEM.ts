import {Service} from "@tsed/di";
import {EventArgs, Guard} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {SystemUser} from "../../../entity/User";


@Service()
export class CHECK_IS_SYSTEM extends Guard<Order> {
  async check(holder: Order, args: EventArgs): Promise<void> {
    const user = args.user;
    if (user !== SystemUser) {
      throw new Error('user is not system');
    }
    // let find = order.offers.find(o => o.performer.id === user.id);
    // if (find) throw new Error('user is not system');
    // if (order.owner.id === user.id) throw new Error('user is not system');
    //
    // if (!user.roles.some(r => r.name == Roles.SYSTEM))
    //   throw new Error('user is not system');

  }


}
