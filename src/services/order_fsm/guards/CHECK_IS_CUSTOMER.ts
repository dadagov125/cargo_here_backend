import {EventArgs, Guard} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {Service} from "@tsed/di";


@Service()
export class CHECK_IS_CUSTOMER extends Guard<Order> {
  async check(order: Order, args: EventArgs): Promise<void> {
    const user = args.user;
    if (order.owner.id !== user.id) throw new Error('user is not customer');
  }

}
