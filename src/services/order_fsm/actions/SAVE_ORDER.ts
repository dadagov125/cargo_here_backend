import {Action, EventArgs} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {OrderRepository} from "../../../repositories/OrderRepository";
import {Service} from "@tsed/di";

@Service()
export class SAVE_ORDER extends Action<Order> {
  constructor(private orderRepository: OrderRepository) {
    super();

  }

  async run(order: Order, args: EventArgs): Promise<void> {
    console.log('SAVE_ORDER', order, args);
    const saved = await this.orderRepository.save(order);
    console.log('AFTER SAVE ORDER', saved);
  }

}
