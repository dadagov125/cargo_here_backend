import {Action, EventArgs} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {OrderRepository} from "../../../repositories/OrderRepository";
import {ServerAction, ServerActionType} from "../../../models/ServerAction";
import {OfferState} from "../../../enums/OfferState";
import {Service} from "@tsed/di";
import {NotificationService} from "../../NotificationService";

@Service()
export class SEND_PERFORMER_SELECTED_PUSH extends Action<Order> {

  constructor(private notificationService: NotificationService, private orderRepository: OrderRepository) {
    super();

  }


  async run(order: Order, args: EventArgs): Promise<void> {
    console.log('SEND_PERFORMER_SELECTED_PUSH', order, args);

    order = await this.orderRepository.findOneOrFail(order.id, {relations: ['offers', 'offers.performer']});

    let selectedOffer = order.offers.find(o => o.state == OfferState.SELECTED)!;
    let performer = selectedOffer.performer;

    await this.notificationService.sendToUser(performer, 'Вас выбрали', 'Заказчик выбрал Вас на выполнения заказа', new ServerAction(ServerActionType.PERFORMER_SELECTED, order.id.toString()));

  }

}
