import {Action, EventArgs} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {OfferState} from "../../../enums/OfferState";
import {Service} from "@tsed/di";

@Service()
export class CANCEL_OFFERS extends Action<Order> {

  async run(order: Order, args: EventArgs): Promise<void> {
    console.log('CANCEL_OFFERS', order, args);
    order.offers.forEach(o => {
      const states = [OfferState.CANCELED, OfferState.REJECTED];
      if (!(o.state in states)) {
        o.state = OfferState.CANCELED;
      }
    })
  }

}
