import {Action, EventArgs} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {OrderRepository} from "../../../repositories/OrderRepository";

import {ServerAction, ServerActionType} from "../../../models/ServerAction";
import {Service} from "@tsed/di";
import {NotificationService} from "../../NotificationService";


@Service()
export class SEND_FOUND_PERFORMERS_PUSH extends Action<Order> {
  constructor(private notificationService: NotificationService, private orderRepository: OrderRepository) {
    super();

  }

  async run(order: Order, args: EventArgs): Promise<void> {

    console.log('FOUND_PERFORMERS', order, args);

    order = await this.orderRepository.findOneOrFail(order.id, {relations: ['foundPerformers', 'foundPerformers.performer', 'offers', 'cargos']});

    let performers = order.foundPerformers.map(f => f.performer);

    for (let p of performers) {
      await this.notificationService.sendToUser(p, 'Новый заказ', 'Предложени о заказе', new ServerAction(ServerActionType.NEW_ORDER, order.id.toString()));
    }
    await this.notificationService.sendToUser(order.owner, 'Найден исполнитель', 'Найдены исполнители заказа', new ServerAction(ServerActionType.FOUND_PERFORMERS, order.id.toString()));

  }

}

