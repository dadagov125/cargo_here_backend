import {Point} from "geojson";
import {Task} from "../../../entity/Task";
import {PointModel} from "../../../models/PointModel";
import {DistanceMatrixResponse} from "@googlemaps/google-maps-services-js/dist/distance";
import {Language, TravelMode, UnitSystem} from "@googlemaps/google-maps-services-js/dist/common";
import {Transport} from "../../../entity/Transport";
import {Cargo} from "../../../entity/Cargo";
import {TaskType} from "../../../enums/TaskType";
import axios, {AxiosResponse} from "axios";
import {TaskState} from "../../../enums/TaskState";
import {Action, EventArgs} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {OrderRepository} from "../../../repositories/OrderRepository";
import {TaskListRepository} from "../../../repositories/TaskListRepository";
import {UserRepository} from "../../../repositories/UserRepository";
import {TransportRepository} from "../../../repositories/TransportRepository";
import {OfferState} from "../../../enums/OfferState";
import {Client} from "@googlemaps/google-maps-services-js/dist";
import {Service} from "@tsed/di";


class MatrixResult {
  /*
  0|1|2|3
  1
  2
  3
   */

  depo: Point;
  indexedTasks: Task[] = [];
  distanceMatrix: number[][] = [];
  durationMatrix: number[][] = [];
}

const matrixPoints = async (depo: Point, tasks: Task[]): Promise<MatrixResult> => {
  const points = [PointModel.fromGeoPoint(depo), ...tasks.map(p => PointModel.fromGeoPoint(p.address.point))];
  const client = new Client();
  const response: DistanceMatrixResponse = await client.distancematrix({
    timeout: 2000,
    params: {
      key: 'AIzaSyBlLviRlav6YY3kn-d8opam5HOsHH5xCjU',
      mode: TravelMode.driving,
      language: Language.ru,
      region: Language.ru,
      units: UnitSystem.metric,
      origins: points,
      destinations: points,
    }
  });

  let data = response.data;
  let rows = data.rows;
  const result = new MatrixResult();
  result.depo = depo;
  for (let i = 0; i < rows.length; i++) {
    let row = rows[i];
    let elements = row.elements;
    result.distanceMatrix[i] = [];
    result.durationMatrix[i] = [];
    //пропускает depo
    if (i > 0) {
      const indexI = i - 1;
      result.indexedTasks[i] = tasks[indexI];
    }
    for (let j = 0; j < elements.length; j++) {
      let element = elements[j];
      let distance = element.distance.value;
      let duration = element.duration.value;
      result.distanceMatrix[i][j] = distance;
      result.durationMatrix[i][j] = duration;

    }
  }
  return result;
};

const calculateTasks = async (transport: Transport, tasks: Task[]) => {

  let matrixResult = await matrixPoints(transport.position, tasks);

  let distanceMatrix = matrixResult.distanceMatrix;

  let groupedByCargo = new Map<Cargo, Task[]>();

  tasks.forEach((item) => {
    let key = item.cargo;
    let collection = groupedByCargo.get(key);
    if (collection) {
      collection.push(item);
    } else {
      groupedByCargo.set(key, [item])
    }
  });

  const pickupDeliveries: Task[][] = [];

  groupedByCargo.forEach((value) => {
    if (value.length === 2) {
      pickupDeliveries.push(
          [value.find(t => t.type === TaskType.PICK_UP)!,
            value.find(t => t.type === TaskType.DELIVERY)!
          ])
    }
  });

  const vrpRequest = {
    depot: 0,
    distanceMatrix,
    pickupsDeliveries: pickupDeliveries.map(t => [matrixResult.indexedTasks.indexOf(t[0]), matrixResult.indexedTasks.indexOf(t[1])])
  };

  let response = await axios.post<number[], AxiosResponse<number[]>>('http://opt:8989/api/vrp', vrpRequest,);

  response.data.forEach(((value, index) => {
    let task = matrixResult.indexedTasks[value];
    if (task) {
      task.index = index;
      const fromIndex = task.index - 1;
      const toIndex = task.index;
      task.distance = matrixResult.distanceMatrix[fromIndex][toIndex];
      task.duration = matrixResult.durationMatrix[fromIndex][toIndex];
      task.state = TaskState.AWAITING;
    }
  }));
};

@Service()
export class SELECT_PERFORMER extends Action<Order> {

  constructor(private orderRepository: OrderRepository,
              private taskListRepository: TaskListRepository,
              private userRepository: UserRepository,
              private transportRepository: TransportRepository) {
    super();
  }


  async run(order: Order, args: EventArgs): Promise<void> {
    console.log('SELECT_PERFORMER', order, args);


    const performerId = Number(args.data?.performerId);

    let performer = await this.userRepository.findOneOrFail(performerId);

    const selectedOrder = await this.orderRepository.findOneOrFail(order.id, {relations: ['offers', 'foundPerformers', 'cargos']});

    order.offers = selectedOrder.offers;
    order.cargos = selectedOrder.cargos;
    order.foundPerformers = selectedOrder.foundPerformers;

    const offer = order.offers.find(o => o.performer.id === performer.id);

    if (!offer) {
      throw new Error(`Отсутствует предложение для исполнителя ${performerId}`)
    }
    if (offer.state !== OfferState.ACCEPTED) {
      throw new Error(`Исполнитель ${performerId} сначало должен принять приглашение`)
    }

    order.offers.forEach(o => {
      const states = [OfferState.CANCELED, OfferState.REJECTED];
      if (o.performer.id !== performerId && !(o.state in states)) {
        o.state = OfferState.CANCELED;
      } else if (o.performer.id === performerId) {
        o.state = OfferState.SELECTED;
      }
    });

    let performerTransport = await this.transportRepository.findOneOrFail({
      where: {
        owner: {
          id: performer.id
        }
      }
    });

    let taskList = await this.taskListRepository.findOneOrFail({
      where: {
        performer: {
          id: performerId
        },
        active: true
      },
      relations: ['orders', 'tasks', 'performer']
    }).catch(() => this.taskListRepository.create({performer: performer, orders: [], tasks: [], active: true}));

    if (!taskList.orders.some(o => o.id === order.id)) {
      taskList.orders.push(order);
    }

    const newTasks = order.cargos
        .map((c) => {

          const pickupTask = new Task();
          pickupTask.type = TaskType.PICK_UP;
          pickupTask.address = c.from;

          pickupTask.state = TaskState.AWAITING;
          pickupTask.cargo = c;
          pickupTask.taskList = taskList;

          const delivreyTask = new Task();
          delivreyTask.type = TaskType.DELIVERY;
          delivreyTask.address = c.to;
          delivreyTask.state = TaskState.AWAITING;
          delivreyTask.cargo = c;
          delivreyTask.taskList = taskList;
          return [pickupTask, delivreyTask];
        }).reduce((p, c) => [...p, ...c]);

    taskList.tasks.push(...newTasks);

    let openedTasks = taskList.tasks.filter(t => {
      return t.state == TaskState.RIDE
          || t.state == TaskState.LATE
          || t.state == TaskState.DROVEUP
          || t.state == TaskState.AWAITING
    });

    await calculateTasks(performerTransport, openedTasks);


    taskList = await this.taskListRepository.save(taskList);

  }

}
