import {Action, EventArgs} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {OfferState} from "../../../enums/OfferState";
import {Service} from "@tsed/di";

@Service()
export class REJECT_MY_OFFER extends Action<Order> {

  async run(order: Order, args: EventArgs): Promise<void> {
    console.log('REJECT_MY_OFFER', order, args);
    order.offers.forEach(o => {
      if (o.performer.id === args.user.id)
        o.state = OfferState.REJECTED;
    })
  }

}


