import {Action, EventArgs} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {Service} from "@tsed/di";
import {MQService} from "../../MQService";

@Service()
export class RUN_SEARCH_PERFORMERS_WORKER extends Action<Order> {
  constructor(private mqService: MQService) {
    super();
  }

  async run(order: Order, args: EventArgs): Promise<void> {
    console.log('RUN_SEARCH_PERFORMERS_WORKER', order, args);
    this.mqService.sendToSearchPerformersQueue(Buffer.from(order.id.toString()));
  }

}
