import {Action, EventArgs} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {ServerAction, ServerActionType} from "../../../models/ServerAction";
import {Service} from "@tsed/di";
import {NotificationService} from "../../NotificationService";

@Service()
export class SEND_ORDER_RUNNING_PUSH extends Action<Order> {

  constructor(private notificationService: NotificationService) {
    super();

  }

  async run(order: Order, args: EventArgs): Promise<void> {
    console.log('SEND_ORDER_RUNNING_PUSH', order, args);

    await this.notificationService.sendToUser(order.owner, 'Заказ выполняется', 'Исполнитель начал Ваш заказ', new ServerAction(ServerActionType.ORDER_RUNNING, order.id.toString()));


  }

}
