import {Action, EventArgs} from "../../fsm/FsmBase";
import {Order} from "../../../entity/Order";
import {ServerAction, ServerActionType} from "../../../models/ServerAction";
import {Service} from "@tsed/di";
import {NotificationService} from "../../NotificationService";

@Service()
export class SEND_NOT_FOUND_PUSH extends Action<Order> {

  constructor(private notificationService: NotificationService) {
    super();

  }

  async run(order: Order, args: EventArgs): Promise<void> {
    console.log('SEND_NOT_FOUND_PUSH', order, args);

    await this.notificationService.sendToUser(order.owner, 'Исполнитель не найден', 'Нет подходящего исполнител', new ServerAction(ServerActionType.NOT_FOUND_PERFORMERS, order.id.toString()));

  }

}

