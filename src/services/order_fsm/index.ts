import {Action, ActionType, EventArgs, FsmBase, Guard, GuardType} from "../fsm/FsmBase"


import {transitions} from "./transitions";
import {OrderFsmEvent} from "./order_fsm_event";
import {OrderFsmState} from "./order_fsm_transition";
import {Order} from "../../entity/Order";
import {Inject, InjectorService, Service} from "@tsed/di";

@Service()
export class OrderFsmService extends FsmBase<Order, OrderFsmState, OrderFsmEvent> {
  @Inject(InjectorService)
  private injector: InjectorService;

  constructor() {
    super(transitions);
  }

  async applyEvent(order: Order, event: OrderFsmEvent, args: EventArgs,): Promise<void> {
    await super.applyEvent(order, event, args);
  }

  async resolveAction(actionType: ActionType<Order>): Promise<Action<Order>> {
    return this.injector.get(actionType)!;
  }

  resolveGuard(guardType: GuardType<Order>): Promise<Guard<Order>> {
    return this.injector.get(guardType)!;
  }

}
