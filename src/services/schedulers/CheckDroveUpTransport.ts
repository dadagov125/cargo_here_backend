import {SchedulerBase} from "./SchedulerBase";
import {OrderRepository} from "../../repositories/OrderRepository";
import {MQService} from "../MQService";
import {TransportRepository} from "../../repositories/TransportRepository";
import {TaskRepository} from "../../repositories/TaskRepository";
import {TaskState} from "../../enums/TaskState";
import {Task} from "../../entity/Task";
import {NotificationService} from "../NotificationService";
import {TaskType} from "../../enums/TaskType";
import {ServerAction, ServerActionType} from "../../models/ServerAction";
import {Service} from "@tsed/di";


@Service()
export class CheckDroveUpTransport extends SchedulerBase {
  constructor(private  orderRepository: OrderRepository,
              private mqService: MQService,
              private transportRepository: TransportRepository,
              private taskRepository: TaskRepository,
              private notificationService: NotificationService
  ) {
    super('*/10 * * * * *');
  }

  async run(): Promise<void> {
    console.log('CheckDroveUpTransport');

    let rideTasks = await this.taskRepository.manager.createQueryBuilder(Task, 'task')
        .innerJoinAndSelect('task.taskList', 'taskList')
        .innerJoinAndSelect('taskList.performer', 'performer')
        .where('task.state IN (:...states)', {states: [TaskState.RIDE, TaskState.LATE]})
        .andWhere('task.duration <=(SELECT EXTRACT(EPOCH FROM (NOW()::TIMESTAMP - "startedTime"::TIMESTAMP)))')
        .getMany();


    let map = rideTasks.map(task => this.transportRepository.findByRadiusWithId(task.taskList.performer.id, task.address.point, 500));

    let transports = await Promise.all(map);


    for (let t of rideTasks) {
      // транспорт находится в радиусе 500метров
      if (transports.some(tp => tp?.owner?.id == t.taskList.performer.id)) {
        t.state = TaskState.DROVEUP;
        this.notificationService.sendToUser(t.taskList.performer,
            'Вы подъехали',
            `Вы подъехали к точке ${t.type == TaskType.PICK_UP ? 'погрузки' : 'доставки'}`,
            new ServerAction(ServerActionType.PERFORMER_DROVEUP, t.taskList.id.toString()));
      } else if (t.state == TaskState.RIDE) {
        t.state = TaskState.LATE;
        this.notificationService.sendToUser(t.taskList.performer,
            'Вы опаздываете',
            `Вы опаздываете к точке ${t.type == TaskType.PICK_UP ? 'погрузки' : 'доставки'}`,
            new ServerAction(ServerActionType.PERFORMER_LATE, t.taskList.id.toString()));
      }
    }

    await this.taskRepository.save(rideTasks);


  }

}
