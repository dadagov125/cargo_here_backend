import {SchedulerBase} from "./SchedulerBase";
import {Service} from "@tsed/di";
import {OrderRepository} from "../../repositories/OrderRepository";
import {OrderState} from "../../enums/OrderState";
import {MQService} from "../MQService";

@Service()
export class SearchPerformersScheduler extends SchedulerBase {
  constructor(private  orderRepository: OrderRepository, private mqService: MQService) {
    super('*/10 * * * * *');
  }

  async run(): Promise<void> {
    console.log('SearchPerformersScheduler');
    this.orderRepository.find({
      where: {
        state: OrderState.SEARCHING
      },
      select: ["id"]

    }).then(searchingOrders => {
      searchingOrders.forEach(o => {
        this.mqService.sendToSearchPerformersQueue(Buffer.from(o.id.toString()));
      });
    }).catch(err => console.log(err))
  }
}
