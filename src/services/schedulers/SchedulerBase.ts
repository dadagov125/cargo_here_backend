import {schedule, ScheduledTask, ScheduleOptions} from "node-cron";
import {OnDestroy, OnInit} from "@tsed/di";

export abstract class SchedulerBase implements OnInit, OnDestroy {
  private task: ScheduledTask;

  constructor(private cronExpression: string, private options?: ScheduleOptions) {
  }

  abstract run(): Promise<void>;

  $onDestroy() {
    this.task?.destroy();
  }

  $onInit() {
    this.task = schedule(this.cronExpression, this.run.bind(this), this.options);
  }
}
