import {User} from "../../entity/User";

export interface StateHolder<S> {
  state: S

  [x: string]: any
}

export abstract class Action<H> {
  abstract run(holder: H, args: EventArgs): Promise<void>;
}

export interface ActionType<H> extends Function {
  new(...args: any[]): Action<H>;
}


export abstract class Guard<H> {
  abstract check(holder: H, args: EventArgs): Promise<void>;
}

export interface GuardType<H> extends Function {
  new(...args: any[]): Guard<H>;
}


export type Target<H, S> = (holder: H, args: EventArgs) => Promise<S>;


export type EventArgs = {
  user: User
  data?: any
}

export interface FsmTransition<H extends StateHolder<S>, S, E> {
  readonly source: S;
  readonly target: S | Target<H, S>;
  readonly events: Array<E>;
  readonly guards?: GuardType<H>[] | undefined;
  readonly actions?: ActionType<H>[] | undefined;
}

export abstract class FsmBase<H extends StateHolder<S>, S, E> {
  constructor(public readonly transitions: FsmTransition<H, S, E>[]) {
  }


  public async applyEvent(stateHolder: H, event: E, args: EventArgs): Promise<void> {
    console.log('applyEvent', stateHolder, event, args);
    await this.executeTransition(stateHolder, event, args);
  }

  private async executeTransition(holder: H, event: E, args: EventArgs): Promise<void> {
    const transition = this.transitions.find(t => t.events.includes(event) && t.source === holder.state);
    if (!transition) {
      throw new Error(`В состоянии ${holder.state} событие ${event} не доступно`)
    }

    await this.callGuards(transition, holder, args);

    const target = (typeof (transition.target) === "function") ? await (transition.target as Target<H, S>).call(transition, holder, args) : transition.target;

    holder.state = target;

    await this.callActions(transition, holder, args);
  }

  abstract resolveAction(actionType: ActionType<H>): Promise<Action<H>>;

  abstract resolveGuard(guardType: GuardType<H>): Promise<Guard<H>>;


  private async callGuards(transition: FsmTransition<H, S, E>, holder: H, args: EventArgs) {
    let guards = transition.guards;
    if (guards) {
      for (let t of guards) {

        let guard = await this.resolveGuard(t);

        await guard.check(holder, args);

      }
    }
  }


  private async callActions(transition: FsmTransition<H, S, E>, holder: H, args: EventArgs) {
    let actions = transition.actions;

    if (actions) {
      for (let a of actions) {
        let action = await this.resolveAction(a);
        await action.run(holder, args);
      }
    }
  }


}
