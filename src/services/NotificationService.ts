import {Service} from "@tsed/di";
import {NotificationRepository} from "../repositories/NotificationRepository";
import {User} from "../entity/User";
import {ServerAction} from "../models/ServerAction";
import {NotificationState} from "../enums/NotificationState";
import {Notification} from "../entity/Notification";
import {MQService} from "./MQService";

@Service()
export class NotificationService {

  constructor(private notificationRepository: NotificationRepository, private mqService: MQService) {
  }

  async sendToUser(to: User, title: string, body: string, action: ServerAction) {
    let notification = this.notificationRepository.create();
    notification.state = NotificationState.NEW;
    notification.title = title;
    notification.body = body;
    notification.action = action;
    notification.to = to;
    notification = await this.notificationRepository.save(notification);
    let sending = this.sendToQueue(notification);

    if (sending) {
      notification.state = NotificationState.SENDING;
      notification = await this.notificationRepository.save(notification);
      return notification
    }
    return notification
  }

  private sendToQueue(notification: Notification): boolean {
    if (notification.state !== NotificationState.NEW) return false;
    return this.mqService.sendToNotificationQueue(Buffer.from(notification.id.toString()));
  }


}
