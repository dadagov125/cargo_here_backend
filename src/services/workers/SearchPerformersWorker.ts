import {WorkerBase} from "./WorkerBase";
import {ConsumeMessage} from "amqplib";
import {TransportRepository} from "../../repositories/TransportRepository";
import {OrderRepository} from "../../repositories/OrderRepository";
import {UserRepository} from "../../repositories/UserRepository";
import {OfferRepository} from "../../repositories/OfferRepository";
import {FoundPerformerRepository} from "../../repositories/FoundPerformerRepository";
import {OrderFsmService} from "../order_fsm";
import {OrderFsmEvent} from "../order_fsm/order_fsm_event";
import {SystemUser} from "../../entity/User";
import {PointModel} from "../../models/PointModel";
import {DistanceMatrixResponse} from "@googlemaps/google-maps-services-js/dist/distance";
import {DistanceMatrixRow, Status, TravelMode, UnitSystem} from "@googlemaps/google-maps-services-js/dist/common";
import {FoundPerformer} from "../../entity/FoundPerformer";
import {Offer} from "../../entity/Offer";
import {OfferType} from "../../enums/OfferType";
import {OfferState} from "../../enums/OfferState";
import {Client} from "@googlemaps/google-maps-services-js/dist";
import {Inject, Service} from "@tsed/di";
import {CONSUMER_CHANNEL} from "../connections/MQ_CONNECTION";


const rearchRadiuses = [3, 5, 8, 13, 21];

@Service()
export class SearchPerformersWorker extends WorkerBase {
  constructor(
      private transportRepository: TransportRepository,
      private orderRepository: OrderRepository,
      private userRepository: UserRepository,
      private offerRepository: OfferRepository,
      private foundPerformerRepository: FoundPerformerRepository,
      private orderFsmService: OrderFsmService,
      @Inject(CONSUMER_CHANNEL) channel: CONSUMER_CHANNEL
  ) {
    super('search_performers', channel);

  }


  async onMessage(msg: ConsumeMessage | null): Promise<void> {

    this.channel.ack(msg!);

    const orderId = Number(msg?.content.toString());


    let order = await this.orderRepository.findOneOrFail(orderId, {relations: ["cargos", "offers", "foundPerformers"]});

    let cargo = order.cargos.sort((a, b) => a.index - b.index)[0];
    let center = cargo.from.point;

    let radiusIndex = 0;

    let transports = await this.transportRepository.findByRadius(center, rearchRadiuses[radiusIndex] * 1000);

    while (transports.length === 0 && radiusIndex < (rearchRadiuses.length - 1)) {
      radiusIndex++;
      transports = await this.transportRepository.findByRadius(center, rearchRadiuses[radiusIndex] * 1000);
    }

    if (transports.length === 0) {
      await this.orderFsmService.applyEvent(order, OrderFsmEvent.ON_NOT_FOUND, {user: SystemUser});
      return;
    }

    let origins = transports.map((t, index) => PointModel.fromGeoPoint(t.position));

    const client = new Client();
    let response: DistanceMatrixResponse = await client.distancematrix({
      timeout: 2000,
      params: {
        key: 'AIzaSyBlLviRlav6YY3kn-d8opam5HOsHH5xCjU',
        origins: Array.from(origins),
        destinations: [PointModel.fromGeoPoint(center)],
        mode: TravelMode.driving,
        language: 'ru',
        region: 'ru',
        avoid: [],
        units: UnitSystem.metric,
        departure_time: Date.now(),
      }
    });
    let responseData = response.data;
    if (responseData.status !== Status.OK) return;
    let rows: DistanceMatrixRow[] = responseData.rows;

    const foundPerformers = rows.map((r, index) => {
      let transport = transports[index];
      let element = r.elements[0];
      const foundPerformer = new FoundPerformer();
      foundPerformer.order = order;
      foundPerformer.performer = transport.owner;
      foundPerformer.distance = element.distance.value;
      foundPerformer.distanceText = element.distance.text;
      foundPerformer.duration = element.duration.value;
      foundPerformer.durationText = element.duration.text;
      return foundPerformer;
    });

    let offers = foundPerformers.map(f => {
      const offer = new Offer();
      offer.performer = f.performer;
      offer.order = f.order;
      offer.type = OfferType.FOR_PERFORMER;
      offer.state = OfferState.AWAITING;
      return offer;
    });

    if (order.foundPerformers && order.foundPerformers.length > 0) {
      await this.foundPerformerRepository.remove(order.foundPerformers);
    }
    if (order.offers && order.offers.length > 0) {
      await this.offerRepository.remove(order.offers);
    }
    order.foundPerformers = foundPerformers;
    order.offers = offers;

    await this.orderFsmService.applyEvent(order, OrderFsmEvent.ON_FOUND, {user: SystemUser});


  }

}
