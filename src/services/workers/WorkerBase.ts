import {OnDestroy, OnInit} from "@tsed/di";
import {ConsumeMessage, Replies} from "amqplib/properties";
import {CONSUMER_CHANNEL} from "../connections/MQ_CONNECTION";
import Consume = Replies.Consume;

export abstract class WorkerBase implements OnInit, OnDestroy {
  private consume: Consume;

  constructor(private queue: string, protected channel: CONSUMER_CHANNEL) {
  }

  abstract onMessage(msg: ConsumeMessage | null): Promise<void>;

  async $onInit() {
    try {
      this.consume = await this.channel.consume(this.queue, this.onMessage.bind(this), {noAck: false});
    } catch (e) {
      process.exit(1);
      console.error(e);
    }

  }

  $onDestroy() {
    this.channel.cancel(this.consume.consumerTag);

  }
}
