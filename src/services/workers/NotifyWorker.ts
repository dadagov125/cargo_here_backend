import {WorkerBase} from "./WorkerBase";
import {ConsumeMessage} from "amqplib";
import {Inject, Service} from "@tsed/di";
import {CONSUMER_CHANNEL} from "../connections/MQ_CONNECTION";
import {NotificationRepository} from "../../repositories/NotificationRepository";
import * as admin from "firebase-admin";
import {DeviceRepository} from "../../repositories/DeviceRepository";
import {NotificationState} from "../../enums/NotificationState";

@Service()
class NotifyWorker extends WorkerBase {
  constructor(@Inject(CONSUMER_CHANNEL) channel: CONSUMER_CHANNEL,
              private notificationRepository: NotificationRepository,
              private deviceRepository: DeviceRepository
  ) {
    super('notification', channel);

  }


  async onMessage(msg: ConsumeMessage | null): Promise<void> {
    console.log('NotifyWorker');
    if (!msg || !msg.content || msg.content.length == 0) return this.channel.ack(msg!);
    try {
      const notifyId = Number(msg?.content.toString());
      let notification = await this.notificationRepository.findOneOrFail(notifyId, {relations: ['to']});

      let device = await this.deviceRepository.findOneOrFail({
        user: {
          id: notification.to.id
        }
      });

      let messaging = admin.messaging();

      let response = await messaging.sendToDevice(device.pushToken,
          {
            notification: {
              title: notification.title,
              body: notification.body,
              clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: notification.action

          },
          {
            priority: 'high'
          });

      if (response.successCount) {
        notification.state = NotificationState.SENT;
        await this.notificationRepository.save(notification);
        response.results.forEach(r => {
          console.info(r);
        });
        return this.channel.ack(msg!);
      } else {
        response.results.forEach(r => {
          console.error(r);
        });
        return this.channel.nack(msg!);
      }


    } catch (e) {
      console.log('NotifyWorker error:', e);
      // this.channel.nack(msg!);
    }


  }

}
