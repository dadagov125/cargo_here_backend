import {Inject, OnDestroy, OnInit, Service} from "@tsed/di";
import {CUSTOMER_CHANNEL} from "./connections/MQ_CONNECTION";


@Service()
export class MQService implements OnInit, OnDestroy {

  public notificationQueue = 'notification';
  public searchPerformersQueue = 'search_performers';

  constructor(@Inject(CUSTOMER_CHANNEL) private channel: CUSTOMER_CHANNEL) {
  }

  sendToNotificationQueue(content: Buffer) {
    return this.channel.sendToQueue(this.notificationQueue, content, {deliveryMode: true});
  }

  async $onInit() {
    await this.initQueues();
  }

  async $onDestroy() {

  }

  sendToSearchPerformersQueue(content: Buffer) {
    return this.channel.sendToQueue(this.searchPerformersQueue, content, {deliveryMode: true});
  }

  private async initQueues() {
    await this.channel.assertQueue(this.notificationQueue, {durable: true});
    await this.channel.assertQueue(this.searchPerformersQueue, {durable: true});
  }
}
