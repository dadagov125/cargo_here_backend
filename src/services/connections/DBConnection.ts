import {Configuration, registerProvider} from "@tsed/di";
import {createConnection} from "@tsed/typeorm";
import {Connection, ConnectionOptions} from "typeorm";

export const DB_CONNECTION = Symbol.for("DB_CONNECTION");
export type DB_CONNECTION = Connection;

registerProvider({
  provide: DB_CONNECTION,
  deps: [Configuration],
  async useAsyncFactory(configuration: Configuration) {
    const settings = configuration.get<ConnectionOptions[]>("typeorm")!;
    const connectionOptions = settings.find(o => o.name === "default");

    return createConnection(connectionOptions!);
  }
});
