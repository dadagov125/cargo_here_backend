import {registerProvider} from "@tsed/di";
import {Channel, connect, Connection} from "amqplib";


export const MQ_CONNECTION = Symbol.for("MQ_CONNECTION");
export type MQ_CONNECTION = Connection

registerProvider({
  provide: MQ_CONNECTION,
  async useAsyncFactory() {
    try {
      const isProd = process.env.NODE_ENV === 'production';
      const host = isProd ? 'mq' : 'localhost';
      return connect(`amqp://${host}:5672`);
    } catch (e) {
      process.exit(0);
    }
  }
});

export const CUSTOMER_CHANNEL = Symbol.for("CUSTOMER_CHANNEL");
export type CUSTOMER_CHANNEL = Channel;
registerProvider({
  provide: CUSTOMER_CHANNEL,
  deps: [MQ_CONNECTION],
  async useAsyncFactory(connection: MQ_CONNECTION) {
    return connection.createChannel();
  }
});

export const CONSUMER_CHANNEL = Symbol.for("CONSUMER_CHANNEL");
export type CONSUMER_CHANNEL = Channel;
registerProvider({
  provide: CONSUMER_CHANNEL,
  deps: [MQ_CONNECTION],
  async useAsyncFactory(connection: MQ_CONNECTION) {
    return connection.createChannel();
  }
});
