import {EntityRepository, Repository} from "typeorm";
import {TaskList} from "../entity/TaskList";

@EntityRepository(TaskList)
export class TaskListRepository extends Repository<TaskList> {

}
