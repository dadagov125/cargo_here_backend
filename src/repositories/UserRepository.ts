import {EntityRepository, Repository} from "typeorm";
import {User} from "../entity/User";
import {Roles} from "../enums/Roles";

@EntityRepository(User)
export class UserRepository extends Repository<User> {


  findOneByExternalId(externalId: string) {
    return super.findOne({
      where: {externalId: externalId},
      cache: false
      //     {
      //   id: `user_findOneByExternalId_${externalId}`,
      //   milliseconds: 60000
      // }
    });
  }



}
