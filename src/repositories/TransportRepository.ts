import {EntityRepository, Repository} from "typeorm";
import {Transport} from "../entity/Transport";
import {Point} from "geojson";


@EntityRepository(Transport)
export class TransportRepository extends Repository<Transport> {

  //this.transportRepository.findByRadius({type:'Point',coordinates:[45.53116058252073, 43.10380892559425]}, 250)
  findByRadius(center: Point, radius: number) {
    const builder = this.manager.createQueryBuilder(Transport, 'tp')
        .innerJoinAndSelect('tp.owner', 'owner')
        .where("st_distance_sphere(tp.position, ST_GeomFromGeoJSON(:center))<=:radius")
        .setParameters({
          center: JSON.stringify(center),
          radius: radius
        });
    return builder.getMany();
  }

  findByRadiusWithId(id: number, center: Point, radius: number) {
    const builder = this.manager.createQueryBuilder(Transport, 'tp')
        .innerJoinAndSelect('tp.owner', 'owner')
        .where('tp.id = :id', {id})
        .andWhere("st_distance_sphere(tp.position, ST_GeomFromGeoJSON(:center))<=:radius")
        .setParameters({
          center: JSON.stringify(center),
          radius: radius
        });
    return builder.getOne();
  }

}
