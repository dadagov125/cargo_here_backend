import {EntityRepository, Repository} from "typeorm";
import {Device} from "../entity/Device";


@EntityRepository(Device)
export class DeviceRepository extends Repository<Device> {

}
