import {EntityRepository, Repository} from "typeorm";
import {FoundPerformer} from "../entity/FoundPerformer";


@EntityRepository(FoundPerformer)
export class FoundPerformerRepository extends Repository<FoundPerformer> {

}
