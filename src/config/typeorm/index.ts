// @tsed/cli do not edit
import * as defaultConfig from './default.config.json';
import * as dockerConfig from './docker.config.json';

const isProd = process.env.NODE_ENV === 'production';
export default [
  (isProd ? dockerConfig : defaultConfig) as any
];
