import {Context, IMiddleware, Middleware, Req} from "@tsed/common";
import {Forbidden, Unauthorized} from "@tsed/exceptions";
import {Request} from "express";
import {User} from "../entity/User";
import {getCustomRepository} from "typeorm";
import {UserRepository} from "../repositories/UserRepository";
import * as admin from 'firebase-admin'


let auth = admin.auth();


@Middleware()
export class FirebaseAuthMiddleware implements IMiddleware {

  private success(req: Request, user: User, authInfo: any) {
    req.user = user;
    req.authInfo = authInfo;
  }

  private async authenticate(req: Request) {
    try {
      let authorization = req.header('Authorization');
      if (!authorization) return new Forbidden('missing authorization header');
      let split = authorization.split(' ');
      if (split[0].toLowerCase() !== 'token') return new Forbidden('support token authorization only');
      const token = split[1];
      if (!token) return new Forbidden('missing authorization token');
      const decoded = await auth.verifyIdToken(token);
      const userRepository = getCustomRepository(UserRepository);
      let user = await userRepository.findOneByExternalId(decoded.uid);
      if (user)
        this.success(req, user, decoded);
      else {
        // userRepository.manager.connection.queryResultCache?.remove([`user_findOneByExternalId_${decoded.uid}`]);
        user = new User();
        user.externalId = decoded.uid;
        user.phone = decoded.phone_number;
        user.email = decoded.email;
        user = await userRepository.save(user);
        this.success(req, user, decoded);
      }
    } catch (e) {
      throw new Forbidden(e.message)
    }
  }

  public async use(@Req() request: Request, @Context() ctx: Context) {

    await this.authenticate(request);

    if (!request.isAuthenticated()) {
      throw new Unauthorized("Unauthorized");
    }
    const options = ctx.endpoint.get(FirebaseAuthMiddleware) || {roles: []};
    const user = request.user as User;

    if (options.roles && options.roles.length > 0) {
      let roles = user.roles;
      for (let role of roles) {
        if (options.roles.some((r: string) => r.toString().toLowerCase() === role.name.toLowerCase()))
          return;
      }
    } else return;
    throw new Forbidden("Forbidden");
  }
}
