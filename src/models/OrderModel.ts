import {Property, Required} from "@tsed/schema";
import {OrderType} from "../enums/OrderType";
import {OrderState} from "../enums/OrderState";
import {OfferModel} from "./OfferModel";
import {CargoModel} from "./CargoModel";
import {FoundPerformerModel} from "./FoundPerformerModel";
import {Order} from "../entity/Order";


export class OrderModel {
  @Property()
  id: number;

  @Property()
  ownerId: number;

  @Property()
  @Required(false)
  departureDate?: Date;

  @Property()
  type: OrderType;

  @Property()
  state: OrderState;

  @Property()
  created: Date;

  @Property()
  price: number;

  @Property()
  offers: OfferModel[];

  @Property()
  cargos: CargoModel[];

  @Property()
  foundPerformers: FoundPerformerModel[];

  public static fromEntity(entity: Order) {
    const model = new OrderModel();
    model.id = entity.id;
    if (entity.owner)
      model.ownerId = entity.owner.id;
    model.departureDate = entity.departureDate;
    model.type = entity.type;
    model.state = entity.state;
    model.created = entity.created;
    model.price = Number(entity.price);
    model.offers = !entity.offers ? [] : entity.offers.map(o => {
      if (!o.order)
        o.order = entity;
      return OfferModel.fromEntity(o)
    });
    model.cargos = !entity.cargos ? [] : entity.cargos.map(o => {
      if (!o.order)
        o.order = entity;
      return CargoModel.fromEntity(o)
    });
    model.foundPerformers = !entity.foundPerformers ? [] : entity.foundPerformers.map(o => FoundPerformerModel.fromEntity(o));
    return model;
  }
}
