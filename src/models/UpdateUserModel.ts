import {Property} from "@tsed/schema";
import {User} from "../entity/User";


export class UpdateUserModel {

  @Property()
  firstname?: string;

  @Property()
  lastname?: string;

  @Property()
  patronymic?: string;



  updateEntity(e: User) {
    e.firstname = this.firstname;
    e.lastname = this.lastname;
    e.patronymic = this.patronymic;
  }
}
