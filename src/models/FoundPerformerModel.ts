import {Property} from "@tsed/schema";
import {FoundPerformer} from "../entity/FoundPerformer";


export class FoundPerformerModel {

  @Property()
  id: number;
  @Property()
  orderId: number;
  @Property()
  performerId: number;
  @Property()
  distance: number;
  @Property()
  distanceText: string;
  @Property()
  duration: number;
  @Property()
  durationText: string;

  public static fromEntity(entity: FoundPerformer) {

    const model = new FoundPerformerModel();
    model.id = entity.id;
    model.orderId = entity.order.id;
    model.performerId = entity.performer.id;
    model.distance = entity.distance;
    model.distanceText = entity.distanceText;
    model.duration = entity.duration;
    model.durationText = entity.durationText;
    return model;
  }
}
