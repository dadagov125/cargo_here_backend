import {Format, JsonFormatTypes, MinItems, Property, Required} from "@tsed/schema";
import {OrderType} from "../enums/OrderType";
import {CreateCargoModel} from "./CreateCargoModel";
import {Order} from "../entity/Order";
import {OrderState} from "../enums/OrderState";

export class CreateOrderModel {
  @Property()
  // @Format(JsonFormatTypes.DATE_TIME)
  @Required(false)
  departureDate?: string;

  @Required()
  @Property()
  type: OrderType;

  @Required()
  @Property()
  price: number;

  @Required()
  @Property()
  @MinItems(1)
  cargos: CreateCargoModel[];

  public static toEntity(model: CreateOrderModel) {
    const order = new Order();
    order.type = model.type;
    order.price = model.price;
    order.state = OrderState.NEW;

    order.departureDate = model.departureDate?new Date(model.departureDate):undefined;

    order.cargos = model.cargos.map((c, index) => {
      const cargo = CreateCargoModel.toEntity(c);
      cargo.index = index;
      return cargo;
    });

    return order;
  }

}
