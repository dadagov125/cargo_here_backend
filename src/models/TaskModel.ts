import {Property} from "@tsed/schema";
import {AddressModel} from "./AddressModel";
import {TaskState} from "../enums/TaskState";
import {TaskType} from "../enums/TaskType";
import {Task} from "../entity/Task";

export class TaskModel {
  @Property()
  id: number;

  @Property()
  taskListId: number;
  @Property()
  cargoId: number;

  @Property()
  address: AddressModel;


  @Property()
  index: number;

  @Property()
  state: TaskState;

  @Property()
  type: TaskType;

  @Property()
  distance: number;

  @Property()
  duration: number;

  @Property()
  startedTime?: Date;

  @Property()
  endedTime?: Date;

  static fromEntity(e: Task) {
    const m = new TaskModel();
    m.id = e.id;
    m.taskListId = e.taskList.id;
    m.cargoId = e.cargo.id;
    m.address = AddressModel.fromEntity(e.address);
    m.index = e.index;
    m.state = e.state;
    m.type = e.type;
    m.distance = e.distance;
    m.duration = e.duration;
    m.startedTime = e.startedTime;
    m.endedTime = e.endedTime;
    return m;
  }
}
