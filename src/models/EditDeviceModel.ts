import {OS} from "../enums/OS";
import {Enum, Property, Required} from "@tsed/schema";
import {Device} from "../entity/Device";


export class EditDeviceModel {


  @Property()
  @Required()
  model: string;

  @Property()
  @Required()
  @Enum(OS)
  os: OS;

  @Property()
  @Required()
  osVersion: string;

  @Property()
  @Required()
  pushToken: string;


  public updateDevice(d: Device) {
    d.model = this.model;
    d.os = this.os;
    d.osVersion = this.osVersion;
    d.pushToken = this.pushToken;
  }
}
