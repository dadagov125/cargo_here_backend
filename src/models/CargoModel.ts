import {Property} from "@tsed/schema";
import {CargoType} from "../enums/CargoType";
import {AddressModel} from "./AddressModel";
import {Cargo} from "../entity/Cargo";

export class CargoModel {
  @Property()
  id: number;

  @Property()
  orderId: number;

  @Property()
  comment: string;

  @Property()
  type: CargoType;

  @Property()
  length: number;

  @Property()
  width: number;

  @Property()
  height: number;

  @Property()
  weight: number;

  @Property()
  from: AddressModel;

  @Property()
  to: AddressModel;

  public static fromEntity(cargo: Cargo) {
    const model = new CargoModel();
    model.id = cargo.id;
    if (cargo.order)
      model.orderId = cargo.order.id;
    model.type = cargo.type;
    model.height = cargo.height;
    model.width = cargo.width;
    model.length = cargo.length;
    model.weight = cargo.weight;
    if (cargo.from)
      model.from = AddressModel.fromEntity(cargo.from);
    if (cargo.to)
      model.to = AddressModel.fromEntity(cargo.to);
    return model;
  }
}
