import {Property} from "@tsed/schema";
import {User} from "../entity/User";

export class UserModel {
  @Property()
  id: number;

  @Property()
  externalId: string;

  @Property()
  roles: string[];

  @Property()
  firstname?: string;

  @Property()
  lastname?: string;

  @Property()
  patronymic?: string;

  @Property()
  phone?: string;

  @Property()
  email?: string;

  static fromEntity(e: User) {
    const m = new UserModel();
    m.id = e.id;
    m.externalId = e.externalId;
    m.roles = e.roles.map(r => r.name);
    m.email = e.email;
    m.phone = e.phone;
    m.firstname = e.firstname;
    m.lastname = e.lastname;
    m.patronymic = e.patronymic;
    return m;
  }


}
