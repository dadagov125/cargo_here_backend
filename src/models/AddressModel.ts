import {Property, Required,} from "@tsed/schema";
import {PointModel} from "./PointModel";
import {Address} from "../entity/Address";

export class AddressModel {

  @Property()
  @Required()
  name: string;

  @Property(PointModel)
  @Required()
  point: PointModel;

  public static toEntity(model: AddressModel) {
    const address: Address = new Address();
    address.name = model.name;
    address.point = PointModel.toGeoPoint(model.point);
    return address;
  }

  public static fromEntity(address: Address) {
    const model = new AddressModel();
    model.name = address.name;
    model.point = PointModel.fromGeoPoint(address.point);
    return model;
  }

}
