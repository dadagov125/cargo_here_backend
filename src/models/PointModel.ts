import {Property, Required} from "@tsed/schema";
import {Point} from "geojson";

export class PointModel {
  @Property()
  @Required()
  lat: number;

  @Property()
  @Required()
  lng: number;


  public static toGeoPoint(model: PointModel): Point {
    return {
      type: "Point",
      coordinates: [model.lng, model.lat]
    }
  }

  public static fromGeoPoint(geopoint: Point): PointModel {
    if (!geopoint || !geopoint.coordinates || geopoint.coordinates.length < 2) throw new Error('invalid coordinates in point');
    const point = new PointModel();
    point.lng = geopoint.coordinates[0];
    point.lat = geopoint.coordinates[1];
    return point;
  }
}
