import {Property} from "@tsed/schema";
import {OrderModel} from "./OrderModel";
import {TaskModel} from "./TaskModel";
import {TaskList} from "../entity/TaskList";

export class TaskListModel {
  @Property()
  id: number;

  @Property()
  orders: OrderModel[];

  @Property()
  tasks: TaskModel[];

  @Property()
  performerId: number;

  @Property()
  active: boolean;

  static fromEntity(e: TaskList) {

    const m = new TaskListModel();
    m.id = e.id;
    m.performerId = e.performer.id;
    m.active = e.active;
    m.orders = e.orders ? e.orders.map(o => OrderModel.fromEntity(o)) : [];
    m.tasks = e.tasks ? e.tasks.map(t => {
      if (!t.taskList)
        t.taskList = e;
      return TaskModel.fromEntity(t);
    }) : [];
    return m;
  }
}
