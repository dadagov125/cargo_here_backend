import {Property, Required} from "@tsed/schema";
import {Transport} from "../entity/Transport";


export class EditTransportModel {
  @Property()
  @Required()
  mark: string;

  @Property()
  @Required()
  model: string;

  @Property()
  @Required()
  regNumber: string;

  @Property()
  @Required()
  maxLength: number;

  @Property()
  @Required()
  maxWidth: number;

  @Property()
  @Required()
  maxHeight: number;

  @Property()
  @Required()
  maxWeight: number;

  public updateTransport(tp: Transport) {
    tp.mark = this.mark;
    tp.model = this.model;
    tp.regNumber = this.regNumber;
    tp.maxHeight = this.maxHeight;
    tp.maxLength = this.maxLength;
    tp.maxWeight = this.maxWeight;
    tp.maxWidth = this.maxWidth;


  }
}
