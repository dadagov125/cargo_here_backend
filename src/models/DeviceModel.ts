import {Property} from "@tsed/schema";
import {OS} from "../enums/OS";

export class DeviceModel {
  @Property()
  id: number;

  @Property()
  userId: number;

  @Property()
  os: OS;

  @Property()
  model: string;

  @Property()
  osVersion: string;
}
