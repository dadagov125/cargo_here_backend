import {Property} from "@tsed/schema";
import {OfferState} from "../enums/OfferState";
import {OfferType} from "../enums/OfferType";
import {Offer} from "../entity/Offer";

export class OfferModel {
  @Property()
  id: number;

  @Property()
  performerId: number;

  @Property()
  orderId: number;

  @Property()
  performerPrice: number;

  @Property()
  state: OfferState;

  @Property()
  type: OfferType;

  @Property()
  created: Date;

  public static fromEntity(offer: Offer) {
    const model = new OfferModel();
    model.id = offer.id;
    model.orderId = offer.order.id;
    model.type = offer.type;
    model.performerId = offer.performer.id;
    model.performerPrice = offer.performerPrice;
    model.state = offer.state;
    model.created = offer.created;

    return model;
  }

}
