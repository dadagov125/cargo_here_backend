import {Property} from "@tsed/schema";

export class ServerAction {
  @Property()
  public type: ServerActionType;
  @Property()
  public payload: string;
  // @Property()
  // click_action = "FLUTTER_NOTIFICATION_CLICK";

  constructor(type: ServerActionType, payload: string) {
    this.type = type;
    this.payload = payload;
  }

  [key: string]: string;

}

export enum ServerActionType {
  ORDER_DONE = "ORDER_DONE",
  NOT_FOUND_PERFORMERS = "NOT_FOUND_PERFORMERS",
  TASK_LIST_DONE = "TASK_LIST_DONE",
  NEW_ORDER = "NEW_ORDER",
  FOUND_PERFORMERS = "FOUND_PERFORMERS",
  OFFER_ACCEPTED = "OFFER_ACCEPTED",
  OFFER_REJECTED = "OFFER_REJECTED",
  OFFER_CANCELED = "OFFER_CANCELED",
  ORDER_RUNNING = "ORDER_RUNNING",
  PERFORMER_SELECTED = "PERFORMER_SELECTED",
  PERFORMER_DROVEUP = "PERFORMER_DROVEUP",
  PERFORMER_LATE = "PERFORMER_LATE"

}
