import {Property, Required} from "@tsed/schema";
import {CargoType} from "../enums/CargoType";
import {AddressModel} from "./AddressModel";
import {Cargo} from "../entity/Cargo";

export class CreateCargoModel {
  @Property()
  @Required()
  type: CargoType;

  @Property()
  comment: string;

  @Property()
  length: number;

  @Property()
  width: number;

  @Property()
  height: number;

  @Property()
  weight: number;

  @Property()
  @Required()
  from: AddressModel;

  @Property()
  @Required()
  to: AddressModel;

  public static toEntity(model: CreateCargoModel) {
    const cargo = new Cargo();
    cargo.type = model.type;
    cargo.length = model.length;
    cargo.width = model.width;
    cargo.height = model.height;
    cargo.weight = model.weight;
    cargo.from = AddressModel.toEntity(model.from);
    cargo.to = AddressModel.toEntity(model.to);
    return cargo;
  }

}
