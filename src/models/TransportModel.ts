import {PointModel} from "./PointModel";
import {Property} from "@tsed/schema";
import {EditTransportModel} from "./EditTransportModel";
import {Transport} from "../entity/Transport";

export class TransportModel extends EditTransportModel {
  @Property()
  id: number;

  @Property()
  ownerId: number;

  @Property()
  position: PointModel;


  static fromEntity(entity: Transport): TransportModel {
    const model = new TransportModel();
    model.id = entity.id;
    model.ownerId = entity.owner.id;
    model.position = PointModel.fromGeoPoint(entity.position);
    model.maxWidth = entity.maxWeight;
    model.maxWeight = entity.maxWeight;
    model.maxLength = entity.maxLength;
    model.maxHeight = entity.maxHeight;
    model.mark = entity.mark;
    model.model = entity.model;
    model.regNumber = entity.regNumber;

    return model;
  }
}
