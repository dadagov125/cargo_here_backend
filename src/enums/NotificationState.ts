export enum NotificationState {
  NEW = "NEW",
  SENDING = "SENDING",
  SENT = "SENT",
  DELIVERED = "DELIVERED",
  READED = "READED"
}
