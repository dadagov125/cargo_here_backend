export enum TaskState {
  AWAITING = "AWAITING",
  RIDE = "RIDE",
  LATE = "LATE",
  DROVEUP = "DROVEUP",
  DONE = "DONE",
  NOT_DONE = "NOT_DONE"
}

