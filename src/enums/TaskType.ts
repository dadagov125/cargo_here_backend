export enum TaskType {
  PICK_UP = "PICK_UP",
  DELIVERY = "DELIVERY"
}
