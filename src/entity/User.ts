import {Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn} from "typeorm";
import {Role} from "./Role";
import {Roles} from "../enums/Roles";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({unique: true, nullable: false})
  externalId: string;


  @ManyToMany(() => Role, {eager: true, nullable: false})
  @JoinTable()
  roles: Role[];

  @Column({nullable: true})
  firstname?: string;

  @Column({nullable: true})
  lastname?: string;

  @Column({nullable: true})
  patronymic?: string;

  @Column({nullable: true, unique: true})
  phone?: string;

  @Column({nullable: true, unique: true})
  email?: string;


}

const SYSTEM_ROLE = new Role();
SYSTEM_ROLE.id = 0;
SYSTEM_ROLE.name = Roles.SYSTEM;

const SYSTEM: User = new User();
SYSTEM.id = 0;
SYSTEM.externalId = "SYSTEM";
SYSTEM.roles = [SYSTEM_ROLE];
SYSTEM.firstname = Roles.SYSTEM;
SYSTEM.lastname = Roles.SYSTEM;
SYSTEM.patronymic = Roles.SYSTEM;
SYSTEM.phone = Roles.SYSTEM;
SYSTEM.email = Roles.SYSTEM;


export const SystemRole = SYSTEM_ROLE;
export const SystemUser = SYSTEM;
