import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Point} from "geojson";


@Entity()
export class Address {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column("geometry", {
    nullable: false,
  })
  point: Point
}
