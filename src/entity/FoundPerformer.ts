import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Order} from "./Order";
import {User} from "./User";

@Entity()
export class FoundPerformer {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Order, {eager: true, nullable: false})
  @JoinColumn()
  order: Order;

  @ManyToOne(() => User, {eager: true, nullable: false})
  @JoinColumn()
  performer: User;

  @Column()
  distance: number;

  @Column()
  distanceText: string;

  @Column()
  duration: number;

  @Column()
  durationText: string;


}
