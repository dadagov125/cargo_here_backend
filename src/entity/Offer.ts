import {Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";
import {OfferState} from "../enums/OfferState";
import {OfferType} from "../enums/OfferType";
import {Order} from "./Order";

@Entity()
export class Offer {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, {eager: true})
  @JoinColumn()
  performer: User;

  @ManyToOne(() => Order, {eager: true})
  @JoinColumn()
  order:Order;

  @Column({type:"decimal", nullable:true})
  performerPrice:number;

  @Column({type:'enum', enum:OfferState})
  state:OfferState;

  @Column({type:'enum', enum:OfferType})
  type:OfferType;

  @CreateDateColumn()
  created:Date;
}
