import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {OS} from "../enums/OS";
import {User} from "./User";

@Entity()
export class Device {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({
    type: "enum",
    enum: OS
  })
  os: OS;

  @Column()
  model: string;

  @Column()
  osVersion: string;


  @Column()
  pushToken: string;

  @OneToOne(() => User)
  @JoinColumn()
  user: User
}
