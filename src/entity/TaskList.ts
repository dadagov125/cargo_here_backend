import {Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Order} from "./Order";
import {Task} from "./Task";
import {User} from "./User";

@Entity()
export class TaskList {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(() => Order, {eager: true})
  @JoinTable()
  orders: Order[];

  @OneToMany(() => Task, task => task.taskList, {eager: true, cascade: true})
  tasks: Task[];

  @ManyToOne(() => User, {eager: true, nullable: false})
  @JoinColumn()
  performer: User;

  @Column({default:false})
  active:boolean

}
