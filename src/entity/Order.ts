import {Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {OrderType} from "../enums/OrderType";
import {OrderState} from "../enums/OrderState";
import {User} from "./User";
import {Offer} from "./Offer";
import {Cargo} from "./Cargo";
import {StateHolder} from "../services/fsm/FsmBase";
import {FoundPerformer} from "./FoundPerformer";

@Entity()
export class Order implements StateHolder<OrderState> {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, {eager: true, nullable: false})
  @JoinColumn()
  owner: User;

  @Column({nullable: true})
  departureDate?: Date;

  @Column({type: 'enum', enum: OrderType})
  type: OrderType;

  @Column({type: 'enum', enum: OrderState})
  state: OrderState;

  @CreateDateColumn()
  created: Date;

  @Column({type: "decimal"})
  price: number;

  @OneToMany(() => Offer, (offer) => offer.order, {cascade: ['insert', 'update']})
  offers: Offer[];

  @OneToMany(() => Cargo, (cargo) => cargo.order, {cascade: ['insert']})
  cargos: Cargo[];

  @OneToMany(() => FoundPerformer, (fp) => fp.order, {cascade: ['insert', 'update', 'remove', 'soft-remove']})
  foundPerformers: FoundPerformer[];
}
