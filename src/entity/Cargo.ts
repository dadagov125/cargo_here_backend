import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {CargoType} from "../enums/CargoType";
import {Address} from "./Address";
import {Order} from "./Order";


@Entity()
export class Cargo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'enum', enum: CargoType, default: CargoType.OTHER})
  type: CargoType;

  @ManyToOne(() => Order, {eager: true})
  @JoinColumn()
  order: Order;

  @Column({nullable: true})
  comment: string;

  @Column({nullable: false})
  index: number;

  @Column({nullable: true})
  length: number;

  @Column({nullable: true})
  width: number;

  @Column({nullable: true})
  height: number;

  @Column({nullable: true})
  weight: number;

  @ManyToOne(() => Address, {eager: true, cascade: ['insert']})
  @JoinColumn()
  from: Address;

  @ManyToOne(() => Address, {eager: true, cascade: ['insert']})
  @JoinColumn()
  to: Address;


  // @Column({type: "simple-array", nullable: true, default: []})
  // imageUrls: string [];


}
