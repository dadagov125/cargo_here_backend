import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {TaskState} from "../enums/TaskState";
import {TaskType} from "../enums/TaskType";
import {Address} from "./Address";
import {TaskList} from "./TaskList";
import {Cargo} from "./Cargo";

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => TaskList, taskList => taskList.tasks)
  @JoinColumn()
  taskList: TaskList;

  @ManyToOne(() => Cargo, {eager: true, nullable: false})
  @JoinColumn()
  cargo:Cargo;

  @ManyToOne(() => Address, {eager: true})
  @JoinColumn()
  address: Address;


  @Column({nullable: false})
  index: number;

  @Column()
  distance:number;

  @Column()
  duration:number;

  @Column({
    type: "enum",
    enum: TaskState
  })
  state: TaskState;

  @Column({
    type: "enum",
    enum: TaskType
  })
  type: TaskType;

  @Column({nullable: true})
  startedTime?: Date;

  @Column({nullable:true})
  endedTime?:Date


}
