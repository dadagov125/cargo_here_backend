import {ValueTransformer} from "typeorm";
import {Point} from "geojson";
import * as wkx from "wkx";

export class PointTransformer implements ValueTransformer {
  to(geojson: Point): string {
    console.log('11111111111', geojson);
    let geometry = wkx.Geometry.parseGeoJSON(geojson);
    let s = geometry.toEwkt();
    let toWkt = geometry.toWkt();
    return s;
  }

  from(wkb: any): Point {
    console.log('22222222', wkb)
    return <Point>wkx.Geometry.parse(new Buffer(wkb, "hex")).toGeoJSON();
  }
}
