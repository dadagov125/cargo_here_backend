import {Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {PointTransformer} from "./transformers/PointTransformer";
import {Point} from "geojson";
import {User} from "./User";

@Entity()
export class Transport {
  @PrimaryGeneratedColumn()
  id: number;



  @ManyToOne(() => User, {eager: true, nullable:false})
  @JoinColumn()
  owner: User;

  @Column()
  mark: string;

  @Column()
  model: string;

  @Column()
  regNumber: string;

  // @Column({type: "simple-array", nullable: false, default: []})
  // imageUrls: string[];

  @Column()
  maxLength: number;

  @Column()
  maxWidth: number;

  @Column()
  maxHeight: number;

  @Column()
  maxWeight: number;

  @Column("geometry", {
    nullable: false,
    // transformer: new PointTransformer()
  })
  position: Point;

}
