import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ValueTransformer
} from "typeorm";
import {NotificationState} from "../enums/NotificationState";
import {ServerAction} from "../models/ServerAction";
import {Property} from "@tsed/schema";
import {User} from "./User";

class NotificationActionValueTransformer implements ValueTransformer {
  from(value: any): any {
    if (!value) return null;
    return new ServerAction(value['type'], value['payload']);
  }

  to(value: any): any {
    return value;
  }
}


@Entity()
export class Notification {

  @Property()
  @PrimaryGeneratedColumn()
  id: number;


  @Property()
  @Column({
    type: "enum",
    enum: NotificationState
  })
  state: NotificationState;


  @Property()
  @CreateDateColumn()
  created: Date;


  @Property()
  @UpdateDateColumn()
  updated: Date;


  @Property()
  @ManyToOne(() => User, {eager: true, nullable: false})
  @JoinColumn()
  to: User;


  @Property()
  @Column({nullable: false})
  title: string;


  @Property()
  @Column({nullable: false})
  body: string;


  @Property()
  @Column({
    type: "jsonb",
    transformer: new NotificationActionValueTransformer(),
  })
  action: ServerAction;

}

