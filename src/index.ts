import {$log} from "@tsed/common";
import {PlatformExpress} from "@tsed/platform-express";
import {Server} from "./Server";

function wait() {
  const sec = 10;
  return new Promise<void>((resolve) => {
    $log.debug(`Wait ${sec} seconds`);
    setTimeout(() => resolve(), sec * 1000);
  });
}

async function bootstrap() {
  try {
    $log.debug("Start server...");
    const isProd = process.env.NODE_ENV === 'production';
    if (isProd)
      await wait();
    const platform = await PlatformExpress.bootstrap(Server);

    await platform.listen();
    $log.debug("Server initialized");
  } catch (er) {
    $log.error(er);
  }
}

bootstrap();
