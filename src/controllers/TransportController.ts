import {BodyParams, Controller, Get, PathParams, Post, Put, Req} from "@tsed/common";
import {Auth} from "../decorators/Auth";
import {User} from "../entity/User";
import {PointModel} from "../models/PointModel";
import {TransportRepository} from "../repositories/TransportRepository";
import {Transport} from "../entity/Transport";
import {EditTransportModel} from "../models/EditTransportModel";
import {TransportModel} from "../models/TransportModel";
import {In} from "typeorm";
import {NotFound} from "@tsed/exceptions";

@Controller("/transport")
export class TransportController {

  constructor(private transportRepository: TransportRepository) {
  }


  @Post("/position")
  @Auth({roles: []})
  async updatePosition(@Req() req: Req, @BodyParams()model: PointModel) {
    const user = req.user as User;

    console.log(model);
    let point = PointModel.toGeoPoint(model);
    await this.transportRepository.manager.createQueryBuilder()
        .update(Transport)
        .set({
          position: point
        })
        .where("ownerId = :ownerId", {ownerId: user.id})
        .execute();
    return "ok";
  }


  @Get("/position/:transportId")
  async getPosition(@Req() req: Req, @PathParams('transportId')transportId: string) {

    let transport = await this.transportRepository.manager.createQueryBuilder(Transport, 'tp')
        .select(['tp.position'])
        .where('tp.id = :id', {id: Number(transportId)})
        .getOneOrFail();
    return PointModel.fromGeoPoint(transport.position);
  }

  @Put()
  @Auth({roles: []})
  async editTransport(@Req() req: Req, @BodyParams() model: EditTransportModel) {
    const user = req.user as User;
    let transport = await this.transportRepository.findOneOrFail({
      where: {
        owner: {
          id: user.id
        }
      }
    }).catch(() => this.transportRepository.create({owner: user, position: PointModel.toGeoPoint({lng: 0, lat: 0})}));

    model.updateTransport(transport);

    const tp = await this.transportRepository.save(transport);
    return TransportModel.fromEntity(tp);
  }

  @Get()
  @Auth({roles: []})
  async getMyTransport(@Req() req: Req) {
    const user = req.user as User;

    const tp = await this.transportRepository.findOneOrFail({
      where: {
        owner: {
          id: user.id
        }
      }
    }).catch(err => {
      throw  new NotFound(err.toString())
    });

    return TransportModel.fromEntity(tp);
  }


  @Get('/:transportId')
  async getTransportById(@Req() req: Req, @PathParams('transportId')transportId: number) {
    const tp = await this.transportRepository.findOneOrFail(transportId)
        .catch(err => {
          throw  new NotFound(err.toString());
        });
    return TransportModel.fromEntity(tp);
  }

  @Post('/allByIds')
  async getAllByIds(@Req() req: Req, @BodyParams() ids: number[]) {
    let transports = await this.transportRepository.findByIds(ids);
    return transports.map((t) => TransportModel.fromEntity(t));
  }

  @Post('/allByOwnerIds')
  async getAllByOwnerIds(@Req() req: Req, @BodyParams() ids: number[]) {
    let transports = await this.transportRepository.find({
      where: {
        owner: {
          id: In(ids)
        }
      }
    });
    return transports.map((t) => TransportModel.fromEntity(t));
  }
}
