import {Controller, Get, PathParams, QueryParams, Req} from "@tsed/common";
import {NotificationRepository} from "../repositories/NotificationRepository";
import {Auth} from "../decorators/Auth";
import {User} from "../entity/User";
import {NotificationState} from "../enums/NotificationState";

@Controller("/notification")
export class NotificationController {

  constructor(private notificationRepository: NotificationRepository) {

  }

  @Get("/")
  @Auth({roles: []})
  getMyAll(@Req() req: Req, @QueryParams('skip') skip: number = 0, @QueryParams('take') take: number = 20,) {
    const user = req.user as User;
    return this.notificationRepository.findAndCount({
      where: {
        to: {
          id: user.id
        },
      },
      skip,
      take
    })
  }


  @Get("/:id/delivered")
  @Auth({roles: []})
  async setDelivered(@Req() req: Req, @PathParams('id') id: number) {
    const user = req.user as User;
    let notification = await this.notificationRepository.findOneOrFail(id, {
      where: {
        to: {
          id: user.id
        }
      }
    });
    notification.state = NotificationState.DELIVERED;
    return this.notificationRepository.save(notification);
  }


  @Get("/:id/readed")
  @Auth({roles: []})
  async setReaded(@Req() req: Req, @PathParams('id') id: number) {
    const user = req.user as User;
    let notification = await this.notificationRepository.findOneOrFail(id, {
      where: {
        to: {
          id: user.id
        }
      }
    });
    notification.state = NotificationState.READED;
    return this.notificationRepository.save(notification);
  }


}
