import {BodyParams, Controller, Get, PathParams, Post, Req} from "@tsed/common";
import {CreateOrderModel} from "../models/CreateOrderModel";
import {OrderRepository} from "../repositories/OrderRepository";
import {User} from "../entity/User";
import {Auth} from "../decorators/Auth";
import {Returns} from "@tsed/schema";
import {OrderModel} from "../models/OrderModel";
import {OrderFsmService} from "../services/order_fsm";
import {OrderFsmEvent} from "../services/order_fsm/order_fsm_event";
import {TransportRepository} from "../repositories/TransportRepository";
import {OfferRepository} from "../repositories/OfferRepository";
import {OfferState} from "../enums/OfferState";
import {ServerAction, ServerActionType} from "../models/ServerAction";
import {Offer} from "../entity/Offer";
import {OfferModel} from "../models/OfferModel";
import {Order} from "../entity/Order";
import {OrderState} from "../enums/OrderState";
import {In} from "typeorm";
import {NotificationService} from "../services/NotificationService";

@Controller("/order")
export class OrderController {

  constructor(
      private offerRepository: OfferRepository,
      private orderRepository: OrderRepository,
      private orderFsmService: OrderFsmService,
      private transportRepository: TransportRepository,
      private notificationService: NotificationService,
  ) {
  }

  @Post('/')
  @Auth({roles: []})
  @Returns(200, OrderModel)
  async create(@Req() req: Req,
               @BodyParams() model: CreateOrderModel) {

    const user = req.user as User;

    let order = CreateOrderModel.toEntity(model);

    order.owner = user;

    order = await this.orderRepository.save(order);

    return await this.getOrder(order.id, req);
  }

  @Auth({roles: []})
  @Get('/:orderId')
  async getOrder(@PathParams("orderId") orderId: number, @Req() req: Req,): Promise<OrderModel> {
    return this.orderRepository.findOneOrFail(orderId, {relations: ['offers', 'cargos', 'foundPerformers']}).then(o => OrderModel.fromEntity(o));
  }

  @Get('/:orderId/events')
  async getAvalableEvents(@PathParams("orderId") orderId: number, @Req() req: Req,) {
    let order = await this.orderRepository.findOneOrFail(orderId);
    return this.orderFsmService.transitions.filter(t => t.source == order.state).map(t => t.events).reduce((p, c) => [...p, ...c]);
  }

  @Auth({roles: []})
  @Post('/:orderId/event/ON_EDIT')
  async onEdit(@PathParams("orderId") orderId: number, @Req() req: Req,) {
    const user = req.user as User;
    let order = await this.orderRepository.findOneOrFail(orderId);
    await this.orderFsmService.applyEvent(order, OrderFsmEvent.ON_EDIT, {user});

    return this.getOrder(order.id, req);

  }

  @Auth({roles: []})
  @Post('/:orderId/event/ON_SEARCH')
  async onSearch(@PathParams("orderId") orderId: number, @Req() req: Req,) {
    const user = req.user as User;
    let order = await this.orderRepository.findOneOrFail(orderId);
    await this.orderFsmService.applyEvent(order, OrderFsmEvent.ON_SEARCH, {user});
    return this.getOrder(order.id, req);
  }

  @Auth({roles: []})
  @Post('/:orderId/event/ON_SELECT')
  async onSelect(@PathParams("orderId") orderId: number, @Req() req: Req, @BodyParams('performerId') performerId: number) {
    const user = req.user as User;
    let order = await this.orderRepository.findOneOrFail(orderId);
    await this.orderFsmService.applyEvent(order, OrderFsmEvent.ON_SELECT, {user, data: {performerId}});
    return this.getOrder(order.id, req);
  }

  @Auth({roles: []})
  @Post('/offer/:offerId/accept')
  async acceptOffer(@PathParams("offerId") offerId: number, @Req() req: Req, @BodyParams('performerPrice') performerPrice: number) {
    const user = req.user as User;

    let offer = await this.offerRepository.findOneOrFail({
      where: {
        id: offerId,
        performer: {
          id: user.id
        }
      },
      relations: ['order', 'order.owner', 'performer']
    });

    if (offer.state !== OfferState.AWAITING) {
      throw new Error('Предложение можно принять только в состоянии AWAITING');
    }
    offer.state = OfferState.ACCEPTED;

    await this.offerRepository.save(offer);

    await this.notificationService.sendToUser(offer.order.owner, 'Заказ принят', 'Исполнитель принял предложение', new ServerAction(ServerActionType.OFFER_ACCEPTED, offer.order.id.toString()));


    return this.getOrder(offer.order.id, req);
  }

  @Auth({roles: []})
  @Post('/offer/:offerId/reject')
  async rejectOffer(@PathParams("offerId") offerId: number, @Req() req: Req) {
    const user = req.user as User;

    // let offer = await this.offerRepository.findOneOrFail({
    //   where: {
    //     id: offerId,
    //     performer: {
    //       id: user.id
    //     }
    //   },
    //   relations: ['order', 'order.owner', 'performer']
    // });

    let offer = await this.offerRepository.manager.createQueryBuilder(Offer, 'offer')
        .innerJoinAndSelect('offer.order', 'order')
        .innerJoinAndSelect('order.owner', 'owner')
        .innerJoinAndSelect('offer.performer', 'performer', 'performer.id = :performer', {performer: user.id})
        .where('offer.id = :offer', {offer: offerId})
        .getOneOrFail();

    if (offer.state !== OfferState.AWAITING) {
      throw new Error('Предложение можно отклонить только в состоянии AWAITING');
    }
    offer.state = OfferState.REJECTED;

    await this.offerRepository.save(offer);


    await this.notificationService.sendToUser(offer.order.owner, 'Заказ отклонен', 'Исполнитель отклонил заказ', new ServerAction(ServerActionType.OFFER_REJECTED, offer.order.id.toString()));

    return this.getOrder(offer.order.id, req);
  }


  @Auth({roles: []})
  @Post('/offer/:offerId/cancel')
  async cancelOffer(@PathParams("offerId") offerId: number, @Req() req: Req) {
    const user = req.user as User;

    // let offer = await this.offerRepository.findOneOrFail({
    //   where: {
    //     id: offerId,
    //     order: {
    //       owner: {
    //         id: user.id
    //       }
    //     }
    //   },
    //   relations: ['order', 'order.owner', 'performer']
    // });

    let offer = await this.offerRepository.manager.createQueryBuilder(Offer, 'offer')
        .innerJoinAndSelect('offer.order', 'order')
        .innerJoinAndSelect('order.owner', 'owner', 'owner.id = :owner', {owner: user.id})
        .innerJoinAndSelect('offer.performer', 'performer')
        .where('offer.id = :offer', {offer: offerId})
        .getOneOrFail();

    console.log('OFFER', offer);
    const cancelableStates = [OfferState.AWAITING, OfferState.ACCEPTED];
    if (!cancelableStates.includes(offer.state)) {
      throw new Error('Предложение не может быть отменено');
    }
    offer.state = OfferState.CANCELED;

    await this.offerRepository.save(offer);

    await this.notificationService.sendToUser(offer.performer, 'Заказ отменен', 'Исполнитель отменил заказ', new ServerAction(ServerActionType.OFFER_CANCELED, offer.order.id.toString()));


    return this.getOrder(offer.order.id, req);
  }


  @Auth({roles: []})
  @Get('/offers/performer/active')
  async getPerformerActiveOffers(@Req() req: Req) {
    const user = req.user as User;

    const offers = await this.offerRepository.manager.createQueryBuilder(Offer, 'offer')
        .innerJoinAndSelect('offer.order', 'order')
        .innerJoinAndSelect('order.owner', 'owner')
        .innerJoinAndSelect('offer.performer', 'performer', 'performer.id = :performer', {performer: user.id})
        .where('offer.state IN (:...statuses)', {statuses: [OfferState.AWAITING, OfferState.ACCEPTED, OfferState.SELECTED]})
        .getMany();

    return offers.map(e => OfferModel.fromEntity(e));
  }

  @Auth({roles: []})
  @Get('/performer/active')
  async getPerformerActiveOrders(@Req() req: Req) {
    const user = req.user as User;

    const orders = await this.offerRepository.manager.createQueryBuilder(Order, 'order')
        .innerJoin('order.offers', 'offers', 'offers.state IN (:...statuses)', {statuses: [OfferState.AWAITING, OfferState.ACCEPTED]})
        .innerJoin('offers.performer', 'performer', 'performer.id = :performer', {performer: user.id})
        .innerJoinAndSelect('order.owner', 'owner')
        .innerJoinAndSelect('order.cargos', 'cargos')
        .innerJoinAndSelect('cargos.from', 'from')
        .innerJoinAndSelect('cargos.to', 'to')

        .getMany();

    return orders.map(e => OrderModel.fromEntity(e));
  }

  @Auth({roles: []})
  @Get('/customer/active')
  async getCustomerActiveOrders(@Req() req: Req) {
    const user = req.user as User;


    const orders = await this.orderRepository.find({
      where: {
        owner: {
          id: user.id
        },
        state: In([OrderState.NEW, OrderState.NO_PERFORMER, OrderState.SEARCHING, OrderState.PERFORMERS_FOUND, OrderState.SELECTED, OrderState.RUNNING])
      },
      relations: ['offers', 'cargos', 'foundPerformers']
    });
    // const orders = await this.orderRepository.manager.createQueryBuilder(Order, 'order')
    //     .innerJoinAndSelect('order.offers', 'offers')
    //     .innerJoinAndSelect('offers.performer', 'performer')
    //     .innerJoinAndSelect('order.owner', 'owner', 'owner.id = :ownerId', {ownerId: user.id})
    //     .innerJoinAndSelect('order.cargos', 'cargos')
    //     .innerJoinAndSelect('cargos.from', 'from')
    //     .innerJoinAndSelect('cargos.to', 'to')
    //     .where('order.state IN (:...states)', {states: [OrderState.NEW, OrderState.NO_PERFORMER, OrderState.SEARCHING, OrderState.PERFORMERS_FOUND, OrderState.SELECTED, OrderState.RUNNING]})
    //     .getMany();

    return orders.map(e => OrderModel.fromEntity(e));
  }

}
