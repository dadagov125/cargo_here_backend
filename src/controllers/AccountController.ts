import {BodyParams, Controller, Get, PathParams, Post, Put, Req} from "@tsed/common";
import {Auth} from "../decorators/Auth";
import {EditDeviceModel} from "../models/EditDeviceModel";
import {User} from "../entity/User";
import {DeviceRepository} from "../repositories/DeviceRepository";
import {DeviceModel} from "../models/DeviceModel";
import {UserRepository} from "../repositories/UserRepository";
import {UserModel} from "../models/UserModel";
import {UpdateUserModel} from "../models/UpdateUserModel";

@Controller("/account")
export class AccountController {
  constructor(private deviceRepository: DeviceRepository, private userRepository: UserRepository) {


  }

  @Get("/echo")
  echo() {
    return 'OK';
  }

  @Get("/authenticate")
  @Auth({roles: []})
  isAuthenticated(@Req() req: Req) {
    return req.user as User
  }

  @Post("/device")
  @Auth({roles: []})
  async registerDevice(@BodyParams() model: EditDeviceModel, @Req() req: Req) {
    const user = req.user as User;
    let device = await this.deviceRepository.findOneOrFail({where: {user: {id: user.id}}, relations: ['user']})
        .catch(() => this.deviceRepository.create({
          user
        }));

    model.updateDevice(device);

    device = await this.deviceRepository.save(device);
    const dm = new DeviceModel();
    dm.id = device.id;
    dm.model = device.model;
    dm.os = device.os;
    dm.osVersion = device.osVersion;
    dm.userId = device.user.id;
    return dm;
  }

  @Get("/device")
  @Auth({roles: []})
  async getDevice(@Req() req: Req) {
    const user = req.user as User;
    return this.deviceRepository.findOneOrFail({where: {user: {id: user.id}}});
  }


  @Post('/allByIds')
  async getByIds(@BodyParams() ids: number[]) {
    let users = await this.userRepository.findByIds(ids);
    return users.map(u => UserModel.fromEntity(u));
  }

  @Get('/:id')
  async getById(@PathParams("id") id: number) {
    let user = await this.userRepository.findOneOrFail(id);
    return UserModel.fromEntity(user);
  }

  @Put('/')
  @Auth({roles: []})
  async updateUser(@BodyParams() model: UpdateUserModel, @Req() req: Req) {
    const user = req.user as User;

    model.updateEntity(user);

    await this.userRepository.save(user);

    return this.getById(user.id);
  }

}
