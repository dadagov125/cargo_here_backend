import {Controller, Get, Req} from "@tsed/common";
import {Auth} from "../decorators/Auth";
import {SystemUser, User} from "../entity/User";
import {TaskListRepository} from "../repositories/TaskListRepository";
import {TaskState} from "../enums/TaskState";
import {OrderState} from "../enums/OrderState";
import {OrderFsmService} from "../services/order_fsm";
import {OrderFsmEvent} from "../services/order_fsm/order_fsm_event";
import {Task} from "../entity/Task";
import {ServerAction, ServerActionType} from "../models/ServerAction";
import {TaskListModel} from "../models/TaskListModel";
import {TaskList} from "../entity/TaskList";
import {Forbidden, NotFound} from "@tsed/exceptions";
import {OrderRepository} from "../repositories/OrderRepository";
import {NotificationService} from "../services/NotificationService";

@Controller("/tasklist")
export class TaskListController {
  constructor(private orderRepository: OrderRepository,
              private orderFsmService: OrderFsmService,
              private notificationService: NotificationService,
              private taskListRepository: TaskListRepository) {
  }

  @Get("/active")
  @Auth({roles: []})
  async getActiveTaskList(@Req() req: Req, activeOnly: boolean = true, id?: number) {
    const user = req.user as User;

    let tasklist = await this._getActiveTaskListEntity(user, activeOnly, id);

    //
    // let tasklist = await this.taskListRepository.findOneOrFail({
    //
    //   where: {
    //     performer: {
    //       id: user.id
    //     },
    //     active: true
    //   },
    //   relations: ['orders', 'orders.cargos', 'tasks', 'performer'],
    // }).catch((err) => {
    //   throw new NotFound(err.toString());
    // });

    return TaskListModel.fromEntity(tasklist);
  }

  @Get("/run")
  @Auth({roles: []})
  async runTask(@Req() req: Req) {
    const user = req.user as User;

    let taskList = await this._getActiveTaskListEntity(user);

    const runningStates = [TaskState.RIDE, TaskState.LATE, TaskState.DROVEUP];

    if (taskList.tasks.some(t => runningStates.includes(t.state)))
      throw new Forbidden('Сначало надо завершить тукущую задачу');

    let task = taskList.tasks.filter(t => t.state == TaskState.AWAITING)
        .sort((a, b) => a.index - b.index)[0];

    task.state = TaskState.RIDE;
    task.startedTime = new Date();

    for (let o of taskList.orders) {
      if (o.state == OrderState.SELECTED) {
        await this.orderFsmService.applyEvent(o, OrderFsmEvent.ON_RUN, {user})
      }
    }

    await this.taskListRepository.save(taskList);
    return this.getActiveTaskList(req);
  }

  @Get("/done")
  @Auth({roles: []})
  async doneTask(@Req() req: Req) {
    const user = req.user as User;

    let taskList = await this._getActiveTaskListEntity(user);

    let task = taskList.tasks.find(t => t.state == TaskState.DROVEUP);

    if (!task) {
      throw new Error('Task in state DROVEUP not found')
    }
    task.state = TaskState.DONE;
    task.endedTime = new Date();

    let groupedByOrder = new Map<number, Task[]>();

    taskList.tasks.forEach((task) => {
      let order = task.cargo.order;
      let collection = groupedByOrder.get(order.id);
      if (collection) {
        collection.push(task);
      } else {
        groupedByOrder.set(order.id, [task])
      }
    });

    console.log('groupedByOrder', groupedByOrder);

    const endedOrderIds: number[] = [];
    const openedTaskStates = [TaskState.AWAITING, TaskState.RIDE, TaskState.LATE, TaskState.DROVEUP];
    for (let order of groupedByOrder.keys()) {
      const orderTasks = groupedByOrder.get(order);

      if (orderTasks?.some(t => t.id === task?.id)
          && !orderTasks.some(t => openedTaskStates.includes(t.state))) {
        endedOrderIds.push(order);
      }
    }
    console.log('endedOrderIds', endedOrderIds);


    const endedOrders = await this.orderRepository.findByIds(endedOrderIds, {relations: ['offers', 'cargos', 'foundPerformers']});


    for (let o of endedOrders) {
      console.log('endedOrder item', o);
      await this.orderFsmService.applyEvent(o, OrderFsmEvent.ON_DONE, {user: SystemUser});
    }

    if (!taskList.tasks.some(t => openedTaskStates.includes(t.state))) {
      taskList.active = false;
    }

    // const endedOrderStates = [OrderState.DONE, OrderState.NOT_DONE, OrderState.CANCELED];
    // if (taskList.orders.every(o => endedOrderStates.includes(o.state))) {
    //   taskList.active = false;
    // }
    taskList = await this.taskListRepository.save(taskList);

    if (!taskList.active) {
      await this.notificationService.sendToUser(user, 'Задачи завершены', 'Все задачи завершено успешно', new ServerAction(ServerActionType.TASK_LIST_DONE, taskList.id.toString()));
    }

    if (taskList.active)
      return this.getActiveTaskList(req);
    else return this.getActiveTaskList(req, false, taskList.id);
  }

  private _getActiveTaskListEntity(user: User, activeOnly: boolean = true, id?: number): Promise<TaskList> {
    let query = this.taskListRepository.manager.createQueryBuilder(TaskList, 'taskList')
        .innerJoinAndSelect('taskList.orders', 'orders')
        .innerJoinAndSelect('orders.cargos', 'cargos')
        .innerJoinAndSelect('cargos.from', 'from')
        .innerJoinAndSelect('cargos.to', 'to')

        .innerJoinAndSelect('orders.offers', 'offers')
        .innerJoinAndSelect('orders.owner', 'owner')
        .innerJoinAndSelect('offers.performer', 'offer_performer')
        .innerJoinAndSelect('offers.order', 'offer_order')


        .innerJoinAndSelect('taskList.tasks', 'tasks')
        .innerJoinAndSelect('tasks.cargo', 'cargo')
        .innerJoinAndSelect('cargo.order', 'cargo_order')
        .innerJoinAndSelect('cargo.from', 'cargo_from')
        .innerJoinAndSelect('cargo.to', 'cargo_to')
        .innerJoinAndSelect('tasks.address', 'address')


        .innerJoinAndSelect('taskList.performer', 'performer', 'performer.id = :performerId', {performerId: user.id});
    if (activeOnly)
      query = query.where('taskList.active = true');

    if (id != null) {
      query = query.andWhereInIds([id]);
    }

    return query.getOneOrFail().catch((err) => {
      throw new NotFound(err.toString());
    });
  }

}
