import {Returns, UseAuth} from "@tsed/common";
import {useDecorators} from "@tsed/core";
import {FirebaseAuthMiddleware} from "../middlewares/FirebaseAuthMiddleware";


export interface IAuthOptions {
  roles?: string[];
}


export function Auth(options: IAuthOptions = {}): Function {
  return useDecorators(
      UseAuth(FirebaseAuthMiddleware, {roles: options.roles}),
      Returns(401, {description: "Unauthorized"}),
      Returns(403, {description: "Forbidden"})
  );
}
