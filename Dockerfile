FROM node:12.19.1-alpine
#RUN apk update && apk add build-base git python

COPY package.json .
#COPY yarn.lock .
COPY ./src ./src
COPY ./tsconfig.json .
COPY ./tsconfig.compile.json .

RUN npm install -g yarn --force
RUN yarn install --production
RUN yarn add tsc
RUN yarn tsc

ENV NODE_ENV production

#ENV GOOGLE_APPLICATION_CREDENTIALS /usr/key/cargohere-31bf3-firebase-adminsdk-xe75u-98aebc0640.json

#ENV PORT ${PORT:-8083}

EXPOSE ${PORT}

CMD ["yarn", "start:prod"]
